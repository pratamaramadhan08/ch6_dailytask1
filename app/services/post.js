const { Post } = require("../models");

module.exports = {
  // for list
  getAllPosts() {
    return Post.findAll();
  },
  // for create
  createPost(title, body) {
    return Post.create({
      title,
      body,
    });
  },
  // for update
  updatePost(pickedPost, newPost) {
    return pickedPost.update(newPost);
  },
  // for setPost
  choosePost(pickedPost) {
    return Post.findByPk(pickedPost);
  },
  // for destroy
  destroyPost(pickedPost) {
    return pickedPost.destroy();
  },
};
